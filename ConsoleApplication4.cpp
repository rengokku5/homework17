﻿
#include <iostream>

class Vector
{
public:
	Vector()
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	
	double Length(double _x, double _y, double _z) 
	{

		double module = x * x + y * y + z * z;
		return sqrt(module);
	}
	
	void Show()
	{
		std::cout << "\n" << x << " " << y << " " << z << "\n";
	}
	
private:
	double x = 5;
	double y = 5;
	double z = 5;
};

int main()
{
	Vector v;
	v.Show();
	std::cout << "The vector length is " << v.Length(5,5,5) << "\n";
}


